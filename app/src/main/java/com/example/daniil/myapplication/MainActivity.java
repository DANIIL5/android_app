package com.example.daniil.myapplication;

import android.content.Intent;
import android.os.AsyncTask;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;
import android.widget.Spinner;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, OnItemSelectedListener {

    Button btnAdd, btnRead, btnClear, btnAuth, contacts, company;
    EditText etLogin, etPassword, etUrl, profile;
    TextView jira;
    public static String LOG_TAG = "my_log";
    String str,str2;
    DBHelper dbHelper;
    Boolean flag, flags, alert;
    Spinner spinner;
    Cursor cursor, cursor2;
    String selection;
    String split;
    SQLiteDatabase database;
    List<String> elements = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnAdd = (Button) findViewById(R.id.btnAdd);


        btnClear = (Button) findViewById(R.id.btnClear);
        btnAuth = (Button) findViewById(R.id.btnAuth);
        contacts = (Button) findViewById(R.id.contacts);
        company = (Button) findViewById(R.id.company);


        //Создаем массив элементов выпадающего списка:
        //elements.add("-");

        flag = false;
        flags = false;


        etLogin = (EditText) findViewById(R.id.etLogin);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etUrl = (EditText) findViewById(R.id.etUrl);

        dbHelper = new DBHelper(this);
        spinner = (Spinner) findViewById(R.id.spinner);
        profile = (EditText) findViewById(R.id.profile);
        jira = (TextView) findViewById(R.id.jira);


        btnClear.setOnClickListener(this);
        contacts.setOnClickListener(this);
        company.setOnClickListener(this);
        btnAdd.setOnClickListener(this);
        btnAuth.setOnClickListener(this);
        database = dbHelper.getReadableDatabase();

        cursor = database.query(DBHelper.TABLE_CONTACTS, null, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            flag = true;
            spinner.setOnItemSelectedListener(this);
            int id = cursor.getColumnIndex(DBHelper.KEY_ID);
            int login = cursor.getColumnIndex(DBHelper.KEY_LOGIN);
            int password = cursor.getColumnIndex(DBHelper.KEY_PASSWORD);
            int url = cursor.getColumnIndex(DBHelper.KEY_URL);
            int Profile = cursor.getColumnIndex(DBHelper.KEY_PROFILE);


            do {
                elements.add(

                        cursor.getString(Profile)
                );
            } while (cursor.moveToNext());

            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, elements);
            dataAdapter.setNotifyOnChange(true);
            //Настраиваем внешний вид выпадающего списка, используя готовый системный шаблон:
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            //Присоединяем адаптер данных к spinner:
            spinner.setAdapter(dataAdapter);
        } else {

            // Toast.makeText(this, "oncreate: " , Toast.LENGTH_LONG).show();
            // Toast.makeText(this, "Пусто2: " , Toast.LENGTH_LONG).show();
            elements.add("Пусто");


            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, elements);
            dataAdapter.setNotifyOnChange(true);
            //Настраиваем внешний вид выпадающего списка, используя готовый системный шаблон:
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            //Присоединяем адаптер данных к spinner:
            spinner.setAdapter(dataAdapter);

            spinner.setOnItemSelectedListener(this);
            flags = true;
        }


        cursor.close();


        //new ParseTask().execute();
    }

    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Spinner spinner = (Spinner) parent;
        if (spinner.getId() == R.id.spinner) {
            //Выбираем элемент выпадающего списка:
            String item = parent.getItemAtPosition(position).toString();

            split = item;
            cursor = database.query(DBHelper.TABLE_CONTACTS, null, DBHelper.KEY_PROFILE + "=" + "'" + split + "'", null, null, null, null);
            if (cursor.moveToFirst()) {


                int login = cursor.getColumnIndex(DBHelper.KEY_LOGIN);
                int password = cursor.getColumnIndex(DBHelper.KEY_PASSWORD);
                int url = cursor.getColumnIndex(DBHelper.KEY_URL);
                int Profile = cursor.getColumnIndex(DBHelper.KEY_PROFILE);
                etUrl.setText(cursor.getString(url));
                etLogin.setText(cursor.getString(login));
                etPassword.setText(cursor.getString(password));
                profile.setText(cursor.getString(Profile));
            }


            //Показываем выбранный элемент с помощью Toast сообщения:
            //Toast.makeText(parent.getContext(), "кликнули: " + split[0], Toast.LENGTH_LONG).show();

        }


    }

    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub


    }

    public static boolean checkWithRegExp(String userNameString) {
        Pattern p = Pattern.compile("^(http|https|)://.*");
        Matcher m = p.matcher(userNameString);
        return m.matches();
    }

    @Override
    public void onClick(View v) {

        String login = etLogin.getText().toString();
        String password = etPassword.getText().toString();
        String url = etUrl.getText().toString();
        String selected = profile.getText().toString();
        SQLiteDatabase database = dbHelper.getWritableDatabase();

        ContentValues contentValues = new ContentValues();


        switch (v.getId()) {

            case R.id.btnAdd:
                //if(split=="Пусто"){
                //   elements.clear();
                //}
                cursor = database.query(DBHelper.TABLE_CONTACTS, null, DBHelper.KEY_LOGIN + "=" + "'" + login + "'", null, null, null, null);
                //cursor.getCount();
                cursor2 = database.query(DBHelper.TABLE_CONTACTS, null, DBHelper.KEY_PROFILE + "=" + "'" + selected + "'", null, null, null, null);
                // cursor2.getCount();
                if (!"".equals(login) && !"".equals(password) && !"".equals(url) && !"".equals(selected)) {
                    alert = false;
                    if (cursor.getCount() == 0) {
                        alert = false;
                        if (cursor2.getCount() == 0) {
                            alert = false;
                            if (checkWithRegExp(url)) {
                                alert = false;
                                contentValues.put(DBHelper.KEY_LOGIN, login);
                                contentValues.put(DBHelper.KEY_PASSWORD, password);
                                contentValues.put(DBHelper.KEY_URL, url);
                                contentValues.put(DBHelper.KEY_PROFILE, selected);
                                database.insert(DBHelper.TABLE_CONTACTS, null, contentValues);
                            } else {
                                alert = true;
                                Toast.makeText(this, "Url,должен содержать http:// или https:// ", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            alert = true;
                            Toast.makeText(this, "Такой профиль уже занят ", Toast.LENGTH_LONG).show();
                        }


                    } else {
                        alert = true;
                        Toast.makeText(this, "Такой логин уже занят ", Toast.LENGTH_LONG).show();
                    }
                } else {
                    alert = true;
                    Toast.makeText(this, "Должны быть заполнены,все поля", Toast.LENGTH_LONG).show();
                }


                cursor = database.query(DBHelper.TABLE_CONTACTS, null, null, null, null, null, null);

                if (cursor.moveToLast()) {


                    int id = cursor.getColumnIndex(DBHelper.KEY_ID);
                    int login2 = cursor.getColumnIndex(DBHelper.KEY_LOGIN);
                    int password2 = cursor.getColumnIndex(DBHelper.KEY_PASSWORD);
                    int url2 = cursor.getColumnIndex(DBHelper.KEY_URL);
                    int Profile2 = cursor.getColumnIndex(DBHelper.KEY_PROFILE);


                    if (flags && !alert) {


                        elements.clear();
                        flags = false;
                    }
                    do {
                        if (!alert) {
                            elements.add(

                                    cursor.getString(Profile2)
                            );
                        }

                        // Toast.makeText(this, "add: " +cursor.getInt(id), Toast.LENGTH_LONG).show();

                    } while (cursor.moveToNext());
                    if (!alert) {
                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                                android.R.layout.simple_spinner_item, elements);
                        dataAdapter.setNotifyOnChange(true);
                        //Настраиваем внешний вид выпадающего списка, используя готовый системный шаблон:
                        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        //Присоединяем адаптер данных к spinner:
                        spinner.setAdapter(dataAdapter);
                    }
                    //flag=true;
                }


                cursor.close();
                break;


            case R.id.btnClear:
                if (split != "Пусто" && split != null) {

                    //Toast.makeText(this, "btnClear:: " + split[0], Toast.LENGTH_LONG).show();
                    database.delete(DBHelper.TABLE_CONTACTS, DBHelper.KEY_PROFILE + "=" + "'" + split + "'", null);
                    // Toast.makeText(this, "btnClear2: " + split[0], Toast.LENGTH_LONG).show();
                }

                // Toast.makeText(this, "Выбрано: " + split[0], Toast.LENGTH_LONG).show();
                cursor = database.query(DBHelper.TABLE_CONTACTS, null, null, null, null, null, null);

                if (cursor.moveToFirst()) {
                    //Toast.makeText(this, "moveToFirst: " + split[0], Toast.LENGTH_LONG).show();
                    elements.clear();
                    flag = true;
                    //spinner.setOnItemSelectedListener(this);
                    int id = cursor.getColumnIndex(DBHelper.KEY_ID);
                    int login2 = cursor.getColumnIndex(DBHelper.KEY_LOGIN);
                    int password2 = cursor.getColumnIndex(DBHelper.KEY_PASSWORD);
                    int url2 = cursor.getColumnIndex(DBHelper.KEY_URL);
                    int Profile2 = cursor.getColumnIndex(DBHelper.KEY_PROFILE);

                    Toast.makeText(this, "id: " + id, Toast.LENGTH_LONG).show();
                    // Toast.makeText(this, "login: " +login2 , Toast.LENGTH_LONG).show();
                    do {
                        elements.add(cursor.getInt(id) +

                                cursor.getString(Profile2)
                        );

                    } while (cursor.moveToNext());

                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                            android.R.layout.simple_spinner_item, elements);
                    dataAdapter.setNotifyOnChange(true);
                    //Настраиваем внешний вид выпадающего списка, используя готовый системный шаблон:
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    //Присоединяем адаптер данных к spinner:
                    spinner.setAdapter(dataAdapter);
                } else {

                    // Toast.makeText(this, "oncreate: " , Toast.LENGTH_LONG).show();
                    elements.clear();
                    // Toast.makeText(this, "Пусто1: " , Toast.LENGTH_LONG).show();
                    elements.add("Пусто");


                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                            android.R.layout.simple_spinner_item, elements);
                    dataAdapter.setNotifyOnChange(true);
                    //Настраиваем внешний вид выпадающего списка, используя готовый системный шаблон:
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    //Присоединяем адаптер данных к spinner:
                    spinner.setAdapter(dataAdapter);


                    flags = true;
                }


                cursor.close();


                break;


            case R.id.btnAuth:


                break;
            case R.id.contacts:

                str="contacts";
                 new ParseTask().execute("contacts");

                //Intent intent = new Intent(MainActivity.this, AboutActivity.class);
                //str = this.Json("contacts");

                //intent.putExtra(AboutActivity.TEXT, str);
                //startActivity(intent);

                break;
            case R.id.company:
                str="company";
                if(str2!=null){
                    Intent intent2 = new Intent(MainActivity.this, AboutActivity.class);

                    intent2.putExtra(AboutActivity.TEXT, str2);
                     startActivity(intent2);

                }
                else{
                    new ParseTask().execute("company");
                }




                break;
        }
        //dbHelper.close();
    }

    private class ParseTask extends AsyncTask<String, Void, String> {

        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        String resultJson = "";
        String login = etLogin.getText().toString();
        String password = etPassword.getText().toString();

        @Override
        protected String doInBackground(String... params) {
            // получаем данные с внешнего ресурса
            try {
                URL url = new URL("http://football.businesschampions.new.rg3.su/jira.php");

                String POST_PARAMS = "param1=" + login + "&param2=" + password + "&param3=" + params[0];
                // POST data


                String USER_AGENT = "Mozilla/5.0";
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("User-Agent", USER_AGENT);
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);
                OutputStream os = urlConnection.getOutputStream();
                os.write(POST_PARAMS.getBytes());
                os.flush();
                os.close();
                // urlConnection.setRequestProperty("Content-Length","" + POST_PARAMS.getBytes());

                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();

                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }

                resultJson = buffer.toString();

            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultJson;
        }

        @Override
        protected void onPostExecute(String strJson) {
            super.onPostExecute(strJson);
            // выводим целиком полученную json-строку
            //jira.setText(strJson);

           // Log.d(LOG_TAG, strJson);
            if(str=="contacts"){
                str = strJson;
                Intent intent = new Intent(MainActivity.this, AboutActivity.class);
                //str = this.Json("contacts");

                intent.putExtra(AboutActivity.TEXT, str);
                startActivity(intent);

            }
            else if(str=="company"){
                str2 = strJson;
                Intent intent = new Intent(MainActivity.this, CompanyActivity.class);
                //str = this.Json("contacts");

                intent.putExtra(AboutActivity.TEXT, str2);
                startActivity(intent);

            }
            //     JSONObject dataJsonObj = null;
            //     String secondName = "";

            //     try {
            //      dataJsonObj = new JSONObject(strJson);
            //        JSONArray friends = dataJsonObj.getJSONArray("friends");

            // 1. достаем инфо о втором друге - индекс 1
            //        JSONObject secondFriend = friends.getJSONObject(1);
            //         secondName = secondFriend.getString("name");
            //         Log.d(LOG_TAG, "Второе имя: " + secondName);

            // 2. перебираем и выводим контакты каждого друга
            //    for (int i = 0; i < friends.length(); i++) {
            //         JSONObject friend = friends.getJSONObject(i);

            //          JSONObject contacts = friend.getJSONObject("contacts");

            //          String phone = contacts.getString("mobile");
            //        String email = contacts.getString("email");
            //          String skype = contacts.getString("skype");

            //         Log.d(LOG_TAG, "phone: " + phone);
            //          Log.d(LOG_TAG, "email: " + email);
            //          Log.d(LOG_TAG, "skype: " + skype);
            //     }

            // } catch (JSONException e) {
            //      e.printStackTrace();
            //  }
        }
    }
 }

