package com.example.daniil.myapplication;

/**
 * Created by YULIA on 17.02.2017.
 */

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;


public class CompanyActivity extends Activity {
    public static final String TEXT = "Text";
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company);
        //объявляем текствью в который выведем текст
        TextView text = (TextView) findViewById(R.id.text);
        //принимаем интент посланый из первой активности
        Bundle extras = getIntent().getExtras();
        //выводим что получили
        text.setText(extras.getString(TEXT));
    }
}