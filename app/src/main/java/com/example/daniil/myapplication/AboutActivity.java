package com.example.daniil.myapplication;

/**
 * Created by YULIA on 17.02.2017.
 */

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class AboutActivity extends Activity {
    public static final String TEXT = "Text";
    String[] items;
    ArrayList<String> listItems;
    ArrayAdapter<String> adapter;
    ListView listView;
    EditText editText;
    JSONObject dataJsonObj;
    String contact_company;
    List<String> elements = new CopyOnWriteArrayList<String>();
    List<String> elements2 = new CopyOnWriteArrayList<String>();
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        //принимаем интент посланый из первой активности
        Bundle extras = getIntent().getExtras();
        //выводим что получили
       // Log.d("mylog", extras.getString(TEXT));
        try {
            dataJsonObj = new JSONObject(extras.getString(TEXT));
            JSONArray contact = dataJsonObj.getJSONArray("contacts");
            //JSONObject contacts = contact.getJSONObject(1);
           // contact_company = contacts.getString("contact_company");
            //Log.d("mylogs1", "NAME COMPANY: " + contact_company);
            for (int i = 0; i <contact.length(); i++) {
                JSONObject contacts = contact.getJSONObject(i);

                elements.add(contacts.getString("contact_company")) ;



            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //elements2=elements;
                    elements2.addAll(elements);

        //Log.d("mylogs11", "NAME COMPANY: " + elements2);
        listView=(ListView)findViewById(R.id.listview);
        editText=(EditText)findViewById(R.id.txtsearch);
        initList();

        editText.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.toString().equals("")){
                    // reset listview
                    elements.clear();
                    elements.addAll(elements2);
                    initList();
                } else {
                    elements.clear();
                    elements.addAll(elements2);
                    initList();
                    // perform search
                    searchItem(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });

    }

    public void searchItem(String textToSearch){
        Iterator<String> it = elements.iterator();

        while(it.hasNext()){
            String value = it.next();

            if(!value.contains(textToSearch)){
                elements.remove(value);
               // Log.d("mylogs111", "NAME COMPANY: " + elements2);
            }
        }

        adapter.notifyDataSetChanged();
    }

    public void initList(){
        //Log.d("mylogs1", "NAME COMPANY: " + elements2);
        //items=new String[]{"Java","JavaScript","C#","PHP", "С++", "Python", "C", "SQL", "Ruby", "Objective-C"};
        //listItems=new ArrayList<>(Arrays.asList(elements));
        adapter=new ArrayAdapter<String>(this, R.layout.list_item, R.id.txtitem, elements);
        listView.setAdapter(adapter);
    }




}